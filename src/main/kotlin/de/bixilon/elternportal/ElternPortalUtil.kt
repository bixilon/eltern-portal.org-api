/*
 * eltern-portal.org API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with art soft and more GmbH, the developer of eltern-portal.org
 */

package de.bixilon.elternportal

import java.net.URISyntaxException
import java.net.URL

object ElternPortalUtil {


    fun checkElternPortalURL(url: String): String {
        if (url.isBlank()) {
            throw IllegalArgumentException("URL is blank!")
        }
        var realURL = url

        realURL = realURL.replace("http://", "https://")
        if (!realURL.startsWith("https://")) {
            realURL = "https://$realURL"
        }

        if ("." !in realURL && "eltern-portal" !in realURL) {
            // just the short code
            realURL = "$realURL.eltern-portal.org"
        }

        realURL = realURL.removeSuffix("/")

        if (!realURL.endsWith(".eltern-portal.org")) {
            throw IllegalArgumentException("URL is not an eltern portal: $url")
        }

        //check if url is parsable
        try {
            URL(realURL).toURI()
        } catch (exception: URISyntaxException) {
            //nope. invalid
            throw exception
        }

        return realURL
    }
}
